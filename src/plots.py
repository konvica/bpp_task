from statsmodels.tsa.seasonal import seasonal_decompose
import matplotlib.pyplot as plt


def plot_decomposition(Y, model="additive"):
    decompose_add = seasonal_decompose(Y, model=model)
    fig = decompose_add.plot()
    return fig
