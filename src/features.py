import glob

import pandas as pd
from statsmodels.tsa.deterministic import CalendarFourier, DeterministicProcess


def read_data(lbmp_dirpath: str, gas_filepath: str, zone="N.Y.C.") -> pd.DataFrame:
    """
    Convenience method, for loading in the data. It is expected to follow the schema of the csv files in the task.
    """
    tmp = []
    for fpath in glob.glob(f"{lbmp_dirpath}/*/*.csv"):
        tmp.append(pd.read_csv(fpath, header=0))
    lbmp = pd.concat(tmp, axis=0)
    lbmp = lbmp[lbmp["Name"] == zone]
    lbmp["ts"] = pd.to_datetime(lbmp["Time Stamp"])
    lbmp["date"] = lbmp["ts"].dt.date

    gas = pd.read_csv(gas_filepath)
    gas["date"] = pd.to_datetime(gas["Date"]).dt.date
    gas["vol"] = gas["Vol."].str.replace("K", "").astype(float).interpolate()

    lbmp = lbmp.merge(gas[["date", "Price", "vol"]], on="date", how="left")
    lbmp = lbmp.fillna(method="ffill").fillna(method="bfill")
    return lbmp


def preprocess_data(input_df: pd.DataFrame) -> pd.DataFrame:
    """
    Convenience method, for preprocessing of the data.
    """
    df = input_df.set_index("ts")[["LBMP ($/MWHr)", "Price", "vol"]]
    df = df.resample("1H").mean()
    df["lbmp"] = df["LBMP ($/MWHr)"].interpolate()
    df["Price"] = df["Price"].interpolate()
    df["vol"] = df["vol"].interpolate()
    df = create_features(df, dropna_start=True)

    return df


def create_features(df: pd.DataFrame, label: str = "lbmp", order: int = 4, dropna_start: bool = False) -> pd.DataFrame:
    """
    Creates time series features from datetime index

    :param df: preprocessed dataframe with `date` column
    :param label: name of target column
    :param order: order parameter for the Fourier terms
    :param dropna_start: convenience method to drop the starting points, where laged values are NaN
    :return: DataFrame with features
    """
    df = df.sort_index()
    df["date"] = df.index
    df["hour"] = df["date"].dt.hour.astype("int8")
    df["dayofweek"] = df["date"].dt.dayofweek.astype("int8")
    df["quarter"] = df["date"].dt.quarter.astype("int8")
    df["month"] = df["date"].dt.month.astype("int8")
    df["dayofyear"] = df["date"].dt.dayofyear.astype("int16")
    df["dayofmonth"] = df["date"].dt.day.astype("int8")
    df["is_wknd"] = (df["date"].dt.weekday // 4).astype("int8")

    df["lag_1"] = df[label].shift(1)
    df["lag_2"] = df[label].shift(2)
    df["lag_24"] = df[label].shift(24)

    fourier = CalendarFourier(freq="H", order=order)
    df = pd.concat((df, fourier.in_sample(df.index)), axis=1)

    dp = DeterministicProcess(df.index, order=0, constant=True)
    df = pd.concat((df, dp.in_sample()), axis=1)

    if dropna_start:
        df = df.iloc[24:]  # since the max lag we are using is 24hours
    return df
