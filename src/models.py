import logging
import numpy as np
import pandas as pd
import statsmodels as sm
from statsmodels.tsa.statespace.sarimax import SARIMAX
from statsmodels.tsa.stattools import adfuller
import xgboost as xgb
from prophet import Prophet
from abc import ABC, abstractmethod


class ModelWrapper(ABC):
    def __init__(self, name=None, cols=None):
        """
        Abstract class for wrapping each model
        """
        self.name = name
        self.cols = cols

    def get_X(self, df):
        """
        Helper function
        """
        return df[self.cols]

    @abstractmethod
    def fit(self, X, Y):
        pass

    @abstractmethod
    def rolling_predict(self, X, Y=None):
        pass


def split(df: pd.DataFrame, k: int = 5, periods: int = 24):
    """
    Yields train and test splits by randomly choosing `k` points in timeline and use previous data as training and
    future `periods` points for test set (e.g. 24hours)
    :param df:
    :param k: number of splits
    :param periods: size of the test split, number steps to predict
    :return: train and test iterator
    """
    cutoffs = np.sort(np.random.randint(24 * 30, df.shape[0] - periods, size=k))  # sorting is not needed
    for i in cutoffs:
        train = df.iloc[0:i]
        test = df.iloc[i : (i + periods)]
        yield train, test


def evaluate(t: np.array, p: np.array) -> pd.Series:
    """
    Unified evaluate method for all models.
    :param t: True values
    :param p: Predicted values
    :return: Series with MAE, MSE, ...
    """
    # add more metrics
    return pd.Series({"mae": np.mean(np.abs(t - p)), "mse": np.mean((t - p) ** 2)})


def cv(
    mod: object, df: pd.DataFrame, k: int, target_col: str, periods: int = 24, include_diff: bool = False
) -> pd.DataFrame:
    """
    Cross validation method. Validates `k` splits for `periods` step forecasts.
    """
    res = []
    for train, test in split(df, k=k, periods=periods):
        X = mod.get_X(train)
        Y = train[target_col]
        mod.fit(X, Y)

        Xtest = mod.get_X(test)
        Ytest = test[target_col].values
        pred = mod.rolling_predict(Xtest)

        metrics = evaluate(Ytest, pred)
        if include_diff:
            metrics["diff"] = Ytest - pred
        res.append(metrics)
    return pd.DataFrame(res)


class XgbWrap(ModelWrapper):
    def __init__(self, feature_cols, name="XGB", verbose=False, **kwargs):
        """
        Wrapper for XGBOOST model forecasting. Mainly needed for the rolling prediction wrapper.

        :param feature_cols: set which cols are used for features
        :param verbose:
        :param kwargs: Parameters for XGBOOST Regressor e.g. num_estimators, max_depth, ...
        """
        self.m = xgb.XGBRegressor(**kwargs)
        self.verbose = verbose
        super().__init__(name=name, cols=feature_cols)

    def __repr__(self):
        return self.name

    def fit(self, X, Y):
        self.m.fit(X, Y, verbose=self.verbose)
        return self

    def predict(self, X):
        return self.m.predict(X)

    def rolling_predict(self, X, Y=None):
        """
        Wrapper to perform step by step recursive prediction for N steps ahead! This strategy uses previous
        prediction as basis for calculating dependent features e.g. lagged values and avoids leaking future information.
        :param X: Dataframe with features
        :param Y: Series with target
        :return: Array with predictions with same length as input X dataframe
        """
        if not isinstance(X, pd.DataFrame):
            raise ValueError("The wrapper expects X as a dataframe with correct column names.")
        N = X.shape[0]
        pred = []
        for j in range(N):
            row = X.iloc[[j]].copy()
            if j > 0:
                row["lag_1"] = pred[j - 1]
            if j > 1:
                row["lag_2"] = pred[j - 2]
            if j > 23:
                row["lag_24"] = pred[j - 24]

            pred.append(self.m.predict(row)[0])
        return np.array(pred)


class SarimaWrap(ModelWrapper):
    def __init__(self, exog_features, name="sarima", **kwargs):
        """
        Wrapper for SARIMAX model forecasting. Mainly needed for unifying the interface.

        :param exog_features: which features to use as exogeneous in sarimax
        :param kwargs: parameters for the SARIMAX model mainly order, seasonal order

        TODO: Not checking if we have required kwargs - order, seasonal order etc.
        """
        self.m = None
        self.arima_params = kwargs
        super().__init__(name=name, cols=exog_features)

    def fit(self, X, Y):
        if adfuller(Y, autolag="AIC")[1] > 0.05:
            logging.warning("Fuller rejects stacionarity.")
        mod = SARIMAX(Y, exog=X, **self.arima_params)
        self.m = mod.fit(disp=False)
        return self

    def rolling_predict(self, X, Y=None):
        """
        The sarimax model forecast uses recursive strategy for multistep prediction by default.
        """
        pred_result = self.m.forecast(steps=X.shape[0], exog=X)
        return pred_result.values

    def __repr__(self):
        return self.name


class ProphetWrap(ModelWrapper):
    def __init__(self, datetime_col, target_col, name="Prophet"):
        """
        Wrapper for Prophet model forecasting. Keep in mind Prophet needs a specific input - dataframe with column
        ts for timestamp and y for column with values

        :param datetime_col: name of date column
        :param target_col: name of target column
        """
        self.datetime_col = datetime_col
        self.target_col = target_col
        self.m = None
        super().__init__(name=name, cols=None)

    def get_X(self, df):
        """
        Prophet is working with dataframes with specific column names, so here we override get_X as a quick fix to the
        different interface of the prophet
        """
        return df.rename({self.datetime_col: "ds", self.target_col: "y"}, axis=1)[["ds", "y"]]

    def fit(self, X, Y=None):
        self.m = Prophet()  # since Prophet can be fitted only once, it is better to initialize it here
        self.m.fit(X)

    def rolling_predict(self, X=None):
        """
        Prophet predict provides prediction as yhat column, but also other info like confidence interval.
        """
        fb_pred_ci = self.m.predict(self.m.make_future_dataframe(periods=X.shape[0], freq="H", include_history=False))
        return fb_pred_ci["yhat"].values
