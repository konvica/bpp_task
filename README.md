# BPP home task - LBMP forecasting

## Requirements
To setup the environment Anaconda or Miniconda is needed. Otherwise please ask me for a pip dependency list.

## Setup
After cloning the repository you can setup the environment with
```shell
conda env create -f env.yml
```

## How to run
The notebook `Experiments.ipynb` contains EDA and forecasting experiments required in the assignment. After setting up the environment, start the `jupyter notebook`
or if you already have running jupyter setup you can just register this new environment kernel and use your jupyter instance. 

Open the notebook, ensure the data are on the readable paths and all code should run as is. 

## Models
I chose to implement 3 approaches:
- SARIMAX
    - Seasonal ARIMA model with exogenneous features
    - Can capture the strong daily seasonality
    - The data seem to be stationary
    - Can use additional features - gas prices
    - Based on ACF and PACF the AR2 process and AR1 with daily seasonality are best suited
    
- XGBOOST regression model
    - classic regression of the price based on time step and lag features
    - benefits from feature engineering and additional features - fourier terms, laged values, time step features, external data (gas prices)
    - the main parameters to tune are num_estimators (helps not overfit), max_depth (complexity of the model)
    
- Prophet from facebook
    - I want to see how the first two models compare with Prophet - autoML forecasting tool
    
## Results
The results from cross-validation put the SARIMA model as the best. But none of the models are able to predict the periods where we see significant changes in the trend. e.g. 2017-08-01 has slighlt higher peak than the prior days. This means there are some hidden effects we are unable to model with the current approach. Best solution would be to explore new datasets to add external information - for example:
- temperature could explain days when people needed heating or AC that can increase the demand for electricity thus increasing the price.
- solar energy efficiency - the days when there is high solar energy supply could decrease the price or days with low solar energy production could increase the price. We could find or collect the data about weather above NYC and deduce how much solar energy is available for the day. 

To see the model prediction results for the day 2017-08-01 look at Chart2.html.

## External data
I used external data - daily Gas prices and volume downlaoded from https://www.investing.com/commodities/natural-gas-historical-data